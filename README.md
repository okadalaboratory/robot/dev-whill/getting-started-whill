# 次世代型電動車椅子 WHILL を使ってみよう

WHILLは、日本発の革新的な電動車椅子およびパーソナルモビリティデバイスのブランドです。
下記のような特徴を持つことで、多くのユーザーから支持を受けており、免許不要で誰でも気軽に乗ることができる近距離モビリティ（ラストOneマイル）として注目されています。

1. デザイン<br>
WHILLの電動車椅子は、伝統的な車椅子やシニアカーとは異なり、モダンでスタイリッシュなデザインが特徴です。
このようなデザインは、ユーザーがよりアクティブになり、外出することを促進するとともに、従来の電動車椅子にありがちな「病院用」のイメージを排除します。

2. 操作性<br>
WHILLの製品は、使いやすい操作パネルや、独自のオムニホイール（全方向に動くホイール）を採用しているため、非常に滑らかで、狭い場所でも容易に操作が可能です。

3. 機動性<br>
WHILLの電動車椅子は、オフロードや不整地での使用も考慮されており、都市部だけでなく、アウトドアでの使用も念頭に置いて設計されています。

4. カスタマイズ性<br>
色やデザインのカスタマイズ、さまざまなアクセサリーの追加など、ユーザーのニーズや好みに合わせてカスタマイズすることが可能です。

5. 安全性<br>
ブレーキやライト、シートベルトなどの安全装置がしっかりと備わっており、ユーザーの安全を考慮して設計されています。

6. 環境への配慮<br>
電動で動くため、環境への配慮も考慮されており、燃料を必要としないことからCO2排出もありません。

<img src="fig/WHILL.png" width="256">


ここでは、WHILLの導入からスマートフォンアプリで走行させるまでを紹介します。

## 最新の情報
2023.10.3<br>
Getting started - WHILLをWHILL Projectのサイトから分離しました。

## 公式情報
[取扱説明書](https://s3.ap-northeast-1.amazonaws.com/dealer.whill.jp/%E4%BB%A3%E7%90%86%E5%BA%97%E6%A7%98%E7%94%A8/002_%E3%83%A1%E3%83%B3%E3%83%86%E3%83%8A%E3%83%B3%E3%82%B9/002_C/Model_C_CK%E5%8F%96%E6%89%B1%E8%AA%AC%E6%98%8E%E6%9B%B8.pdf)

[よくある質問](https://whill.inc/jp/whill-contact/model-cr#contactModelQAList)

[仕様](https://whill.inc/jp/whill-contact/model-cr#supportModelSpec)

[修理の申し込み](https://whill.inc/jp/whill-contact#content-form-private-customer-repair-request)

[アクセサリ購入](https://jp-store.whill.inc/accessories/accessories-c.html)

[ソフトウェアライブラリ](https://github.com/WHILL)

[技術サポート](https://github.com/WHILL/Model_CR_Technical_Support)

[紹介動画](https://whill.inc/jp/model-cr#introductionMovie)

[アプリのダウンロード](https://whill.inc/jp/whill-contact/model-cr#supportModelApp)


## 準備
### 組立と分解


　
### 充電
<img src="fig/caution.png" width="128"><br>
長期間使用しない場合でもバッテリーは充電して下さい。そのまま放置すると過放電状態になり、バッテリーが使えなくなります。

バッテリーを本体に取り付けたまま充電する場合、安全のため本体の電源が入りません。

バッテリーの充電には約５時間必要です。余裕を持って充電しておきましょう。


## WHILLに乗ってみよう
### 電源を入れる
電源ボタンを押すと、WHILLの電源が入ります。<br>
バッテリーの残量が表示されるので確認して下さい。満充電になると表示は100になります。

### 最高速度を設定する
最高速度は速度調整スイッチで1（遅い）から　４（早い）の４段階で調整できます。<br>
慣れないうちは1（遅い）に設定して下さい。

### 運転する
コントローラを前後左右に動かすことで、任意の方向に移動することができます。



## WHILLをアプリで動かしてみよう
スマートフォンのアプリでWHILLを簡単に動かすことができます。iPad あるいは iPhone の App storeでWHILLアプリをダウンロードして下さい。<BR>
<img src="fig/app1.png" width="256">

ダウンロードしたアプリを起動し、「接続を開始」ボタンを押して下さい。<br>
ログインIDとパスワードの入力を求められるので、マニュアルに記載の情報(椅子のクッションを持ち上げると書いてあります)を入力し、「OK」ボタンを押すとWHILLに接続されます。<br>
<img src="fig/app2.png" width="128">
<img src="fig/app3.png" width="128">

WHILLに接続されると、現在の状態（ステータス）が表示されます。<br>
画面の下のアイコンをタッチすることで、「ステータス画面」、「コントローラー画面」、「モード」画面に移動します。<br>

ステータス画面で「詳しい情報」ボタンをクリックすると、バッテリーの残量や消耗度などの詳細情報が表示されます。<br>
<img src="fig/app5.png" width="128">
<img src="fig/app.png" width="128">

ステータス画面ではWHILLのロック／解錠ができます。WHILLから離れるときはロックしておくことをお勧めします。<br>
<img src="fig/app6.png" width="128">
<img src="fig/app4.png" width="128">

コントローラー画面ではジョイスティックが画面上の表示され、実機のWHILLと同様に動かすことができます。周囲の状況に注意して操作して下さい。<br>
<img src="fig/app7.png" width="128">

モード画面ではWHILLの最高速度の設定を、「のんびり」、「標準設定」、「きびきび」のいずれかに変更できます。<br>
標準設定では前進最高速度が 1:1.0km/h、2:2.0km/h、3:4.0km/h、4:6.0km/h に設定されており、WHILL本体のボタンで1〜4を選ぶことができます。<br>
<img src="fig/app8.png" width="128">

## Q&A
[Q.運転するために、免許は必要ですか？](https://whill.inc/jp/guide)<br>
A:免許は必要ありません。道路交通法では歩行者の扱いとなります。
歩行者としての交通ルール、マナーを守って歩道を走行してください。

[Q.最高速度はどれくらいですか？](https://whill.inc/jp/guide)<br>
A:最高速度は6km/hです。早歩きと同じぐらいのスピードです。
設定を変更することによって、最高速度をよりゆっくりな速度に制限することも可能です。

[Q.一回の満充電でどれくらい走行できますか？](https://whill.inc/jp/guide)<br>
A:Model C2/CK2は、理想的な状況で最大約18km走行可能です。
走行可能距離は、路面状況（坂道など）、カーブ、荷重、温度などにより変動します。




## リンク
[WHILL 公式サイト](https://whill.inc/jp/)

[Model CR（研究開発用）](https://whill.inc/jp/whill-contact/model-cr)


## おまけ
<img src="fig/WHILL414.jpeg" width="256">

|№|色|値|
|:-:|:-:|:-:|
|1<td bgcolor=white>white|#ffffff|
|2<td bgcolor=black><font color=white>black|#000000|
|3<td bgcolor=red><font color=white>red|#0000ff|
|4<td bgcolor=blue><font color=white>blue|#ffff00|
|5<td bgcolor=yellow>yellow|#ffff00|
|6<td bgcolor=green><font color=white>green|#ffff00|
|7<td bgcolor=orange>orange|#ffff00|
|8<td bgcolor=pink>pink|#ffff00|
|９<td bgcolor=graygray>gray|#ffff00|

| 列 1 | 列 2 (左寄せ) | 列 3 (中央) | 列 4 (右寄せ) |
|------|:--------------|:-----------:|--------------:|
| 1    | Apple         | US          |      $604.30B 
| 2    | Google        | US          |      $518.92B 
| 3    | Microsoft     | US          |      $436.83B 
{.left}